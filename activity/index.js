//console.log("hellow world")

//3
fetch('https://jsonplaceholder.typicode.com/todos',  {

	method: 'GET',

	headers: {
		'Content-Type': 'application/json'
	},


})

.then((response)=> response.json())
.then((json) => console.log(json))

//4
fetch('https://jsonplaceholder.typicode.com/todos',  {

method: 'GET',

headers: {
	'Content-Type': 'application/json'
},


})

.then((response)=> response.json())
.then(function(json) {
	let newArr = json
	let newArr2 = newArr.map(function(item) {
		return `${item.title}`
	})

	console.log(newArr2)

})



//5
fetch('https://jsonplaceholder.typicode.com/todos/4',  {

	method: 'GET',

	headers: {
		'Content-Type': 'application/json'
	},


})

.then((response)=> response.json())
.then((json) => console.log(json))

//6
async function fetchData () {
	let response =  await fetch('https://jsonplaceholder.typicode.com/posts/15', {
	method: 'GET'})
	let json = await response.json()
	console.log(`Status: ${response.status}, Title: ${json.title}`)


}

fetchData();


//7
fetch('https://jsonplaceholder.typicode.com/todos',  {

	method: 'POST',

	headers: {
		'Content-Type': 'application/json'
	},

	body: JSON.stringify({
		    "title": "create to do list",
		    "description":"i will create to do list",
		     "userId":"201"

	})

})

.then((response)=> response.json())
.then((json) => console.log(json))

//8
fetch('https://jsonplaceholder.typicode.com/todos/200',  {

	method: 'PUT',

	headers: {
		'Content-Type': 'application/json'
	},

	body: JSON.stringify({
		    "title": "Update to do list",
		    "description":"i will update to do list"
		     
	})
})

.then((response)=> response.json())
.then((json) => console.log(json))


//10
fetch('https://jsonplaceholder.typicode.com/todos/200',  {

	method: 'PUT',

	headers: {
		'Content-Type': 'application/json'
	},

	body: JSON.stringify({
		    "title": "my title is PUT",
		    "description":"my description is PUT",
		    "status":"my status is PUT",
		    "date-completed" :"my status is PUT",
		    "userId": "200"
		    
	})

})

.then((response)=> response.json())
.then((json) => console.log(json))

fetch('https://jsonplaceholder.typicode.com/todos/200',  {

	method: 'PATCH',

	headers: {
		'Content-Type': 'application/json'
	},

	body: JSON.stringify({
		    "title": "my title is PATCH",
		    "description":"my description is PATCH",
		    "status":"my status is PATCH",
		    "date-completed" :"my status is PATCH",
		    "userId": "200"
		    
	})

})

.then((response)=> response.json())
.then((json) => console.log(json))

//11

let dateNow = new Date().toJSON()
fetch('https://jsonplaceholder.typicode.com/todos/200',  {

	method: 'PATCH',

	headers: {
		'Content-Type': 'application/json'
	},

	body: JSON.stringify({

		    "completed": true,
		    "date-completed": 'JAN 31 2022' 
	})

})

.then((response)=> response.json())
.then((json) => console.log(json))


//12
fetch('https://jsonplaceholder.typicode.com/todos/1', {

	method: 'DELETE'

});