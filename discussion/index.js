//console.log(`hello world`)


//javascipt  synchronous vs asynchronous

/*
asynchronous
	means that we can procede to execute other statements, while time consuming code is running in the background 

*/

//fetch() method 
// returns  a promise that resloves to a "response object"
//promise - object that represent the eventual completion (or failure) of an asynchronous function and its resulting value
console.log(fetch('https://jsonplaceholder.typicode.com/posts'))

//fetch() method 
// returns  a promise that resloves to a response object
//promise - object that represent 
fetch('https://jsonplaceholder.typicode.com/posts')
//then() capture the" response object" and returns another promise which is will eventually be resolve or rejected 
.then(response => console.log(response.status));
//the out put is "200" means it is ok

fetch('https://jsonplaceholder.typicode.com/posts')
// json() - from the "response object" to conert the data retrieved into JSON format to be used in our application
//.then(response => response.json())
//prin the converted JSON valuse from the "fetch" request
//.then((json) => console.log(json));
//"promise chain" - using multiple .then()

//.then(response => console.log(response.json()));


//asyc and await 

async function fetchData(){
	let result = await fetch('v')
	console.log(result);
	console.log(typeof result);
	console.log(result.body);

	let json = await result.json();
	console.log(json);
};
//fetchData();

//creating a post
fetch('https://jsonplaceholder.typicode.com/posts',  {
	//sets the metohd of the "request object"
	method: 'POST',
	//set the header data of the "request object" to be sent to the back-end
	headers: {
		'Content-Type': 'application/json'
	},
	//JSON.stringfy - convert the object into a stringfy JSON
	body: JSON.stringify({
		    "userId": 1,
		    "title": "create post",
		    "body": "create post File"
	})

})

.then((response)=> response.json())
.then((json) => console.log(json))

//put 
fetch('https://jsonplaceholder.typicode.com/posts/1',  {
	//sets the metohd of the "request object"
	method: 'PUT',
	//set the header data of the "request object" to be sent to the back-end
	headers: {
		'Content-Type': 'application/json'
	},
	//JSON.stringfy - convert the object into a stringfy JSON
	body: JSON.stringify({
		    "userId": 1,
		    "title": "need update",
		    "body": "got updated File"
	})

})

.then((response)=> response.json())
.then((json) => console.log(json));

//delete
fetch('https://jsonplaceholder.typicode.com/posts/1', {

	method: 'DELETE'

});

































